<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
  <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js">
  </script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.js">
  </script>
  <title>Sign up for new student</title>
  <style>
    .background {
      width: 35rem;
      margin: auto;
      margin-top: 1rem;
      padding: 0.6rem 0.8rem;
      align-items: center;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .signup {
      display: flex;
      padding: 3rem 2.8rem;
      align-items: center;
      justify-content: center;
    }

    .signup-form {
      display: flex;
      width: 100%;
      font-size: 18px;
    }

    .signup-form-text {
      width: 8rem;
      padding: 0.4rem 0.6rem;
      margin-right: 1rem;
      text-align: center;
      color: white;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .input[type="text"] {
      width: 20rem;
      height: 2.5rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .gender {
      display: flex;
      width: 130px;
      align-items: center;
      padding-bottom: 0.5rem;
    }

    select {
      width: 40%;
      outline: none;
      height: 2.8rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .input-birth {
      width: 40%;
      height: 2.6rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .signup-button {
      justify-content: center;
      margin-top: 2rem;
    }

    input[type="submit"] {
      height: 2.8rem;
      width: 8rem;
      cursor: pointer;
      color: white;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .input_img {
      margin-top: 0.5rem;
    }
  </style>
</head>

<body>
  <div class="background">
    <?php
    $fullName = $gender = $department = $birth = "";
    if ($_SERVER["REQUEST_METHOD"] == "POST") {
      $check = 0;
      if (empty(checkInput($_POST["fullName"]))) {
        echo "<div style='color: red;'>Hãy nhập tên</div>";
        $check++;
      }
      if (empty($_POST["gender"])) {
        echo "<div style='color: red;'>Hãy chọn giới tính</div>";
        $check++;
      }
      if (empty(checkInput($_POST["department"]))) {
        echo "<div style='color: red;'>Hãy chọn phân khoa</div>";
        $check++;
      }

      if (empty(checkInput($_POST["birth"]))) {
        echo "<div style='color: red;'>Hãy nhập ngày sinh</div>";
        $check++;
      } elseif (!validateDate($_POST["birth"])) {
        echo "<div style='color: red;'>Hãy nhập ngày sinh đúng định dạng</div>";
        $check++;
      }
      if (!file_exists('upload')) {
        mkdir('upload', 0777, true);
      }
      if (!empty($_FILES['image'])) {
        $acceptable = array(
          'image/jpeg',
          'image/png',
          'image/jpg'
        );

        if (!in_array($_FILES['image']['type'], $acceptable) && (!empty($_FILES["image"]["type"]))) {
          echo "<div style='color: red;'>Hình ảnh tải lên phải đúng định dạng .JPEG, .PNG hoặc .JPG</div>";
          $check++;
        } else {
          $filename = $_FILES["image"]["tmp_name"];

          $originalName = pathinfo($_FILES["image"]["name"], PATHINFO_FILENAME);
          $extension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);

          $img = "upload/" . $originalName . "_" . getStringOfDate() . "." . $extension;
          move_uploaded_file($filename, $img);
          $_POST['image'] = $img;
        }
      }
      if ($check == 0) {
        session_start();
        $_SESSION = $_POST;
        header("Location: ./img_confirm.php");
      }
    }
    function checkInput($data)
    {
      $data = trim($data);
      $data = stripslashes($data);
      return $data;
    }
    function validateDate($date, $format = 'd/m/Y')
    {
      $d = DateTime::createFromFormat($format, $date);
      return $d && $d->format($format) == $date;
    }
    function getStringOfDate()
    {
      $date   = new DateTime();
      $result = $date->format('Y-m-d-H-i-s');
      $krr    = explode('-', $result);
      $result = implode("", $krr);
      return $result;
    }

    ?>
    <div class="signup">
      <form action="img_signup.php" method="POST" id="form" enctype="multipart/form-data">
        <div class="signup-form">
          <p class="signup-form-text">
            Họ và tên
            <span style="color: red">*</span>
          </p>
          <input name="fullName" type="text" class="input">
        </div>
        <div class="signup-form">
          <p class="signup-form-text">
            Giới tính
            <span style="color: red">*</span>
          </p>
          <div class="gender">
            <?php
            $gender = array(0 => "Nam", 1 => "Nữ");
            for ($i = 0; $i < count($gender); $i++) {
              echo '
                      <input type="radio" id="' . $i . '" name="gender" value="' . $gender[$i] . '">
                  ';
              echo '
                      <label for="' . $i . '" style="margin: 0 20px 4px 6px">' . $gender[$i] . '</label> 
                  ';
            }
            ?>
          </div>
        </div>
        <div class="signup-form">
          <p class="signup-form-text">
            Phân khoa
            <span style="color: red">*</span>
          </p>
          <select name="department">
            <?php
            $department = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
            foreach ($department as $key => $value) {
              echo '<option >' . $value . '</option>';
            }
            ?>
          </select>
        </div>
        <div class="signup-form">
          <p class="signup-form-text">
            Ngày sinh
            <span style="color: red">*</span>
          </p>
          <input type="text" name="birth" id="birth" class="input-birth" placeholder="dd/mm/yyyy">
        </div>
        <div class="signup-form">
          <p class="signup-form-text">
            Địa chỉ
          </p>
          <input type="text" name="address" id="address" class="input">
        </div>
        <div class="signup-form">
          <p class="signup-form-text">
            Hình ảnh
          </p>
          <input type="file" name="image" id="img" class="input_img">
        </div>
        <div class="signup-form signup-button">
          <input type="submit" value="Đăng Ký">
        </div>
      </form>
    </div>
    <script type="text/javascript">
      $(".birth").datepicker({
        format: "dd/mm/yyyy",
      });
    </script>
  </div>
</body>

</html>