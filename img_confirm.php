<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Sign up for new student</title>
  <style>
    .background {
      width: 20rem;
      margin: auto;
      margin-top: 2rem;
      padding: 0.6rem 5.8rem;
      align-items: center;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .signup {
      display: flex;
      padding: 0rem 2.8rem;
      align-items: center;
      justify-content: center;
    }

    .signup-form {
      display: flex;
      width: 100%;
      font-size: 18px;
    }

    .signup-form-text {
      width: 8rem;
      padding: 0.4rem 0.6rem;
      ;
      margin-right: 1rem;
      text-align: center;
      color: white;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .input[type="text"] {
      width: 15rem;
      height: 2.6rem;
      padding-top: 1.5rem;
    }

    .gender {
      display: flex;
      width: 130px;
      align-items: center;
      padding-bottom: 0.5rem;
    }

    select {
      width: 40%;
      outline: none;
      height: 2.8rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .input-birth {
      width: 40%;
      height: 2.6rem;
      padding-left: 0.5rem;
      border-radius: 8px;
      border: 2px solid #4e7aa3;
    }

    .signup-button {
      display: flex;
      justify-content: center;
      margin-top: 2rem;
    }

    input[type="submit"] {
      height: 2.8rem;
      width: 8rem;
      cursor: pointer;
      color: white;
      background-color: #5b9bd5;
      border-radius: 8px;
      border: solid 2px #4e7aa3;
    }

    .input_img {
      margin-top: 1rem;
    }
  </style>
</head>

<body>
  <?php
  session_start();
  $fullName = $_SESSION['fullName'];
  $gender = $_SESSION['gender'];
  $department = $_SESSION['department'];
  $birth = $_SESSION['birth'];
  $address = $_SESSION['address'];
  $img = $_SESSION['image'];
  ?>
  <div class="background">
    <div class="signup">
      <form method="POST" id="form" enctype="multipart/form-data">
        <div class="signup-form">
          <p class="signup-form-text">
            Họ và tên
          </p>
          <div type="text" class="input" name="fullname">
            <?php
            echo $fullName;
            ?>
          </div>

        </div>

        <div class="signup-form">
          <p class="signup-form-text">
            Giới tính
          </p>
          <div type="text" class="input">
            <?php
            echo $gender;
            ?>
          </div>
        </div>

        <div class="signup-form">
          <p class="signup-form-text">
            Phân khoa
          </p>
          <div type="text" class="input">
            <?php
            echo $department;
            ?>
          </div>
        </div>

        <div class="signup-form" date-date-format="dd/MM/yyyy">
          <p class="signup-form-text">
            Ngày sinh
          </p>
          <div type="text" class="input">
            <?php
            echo $birth;
            ?>
          </div>
        </div>

        <div class="signup-form">
          <p class="signup-form-text">
            Địa chỉ
          </p>
          <div type="text" class="input">
            <?php
            echo $address;
            ?>
          </div>
        </div>

        <div class="signup-form">
          <p class="signup-form-text">
            Hình ảnh
          </p>
          <div type="text" class="input_img">
            <?php
            echo '<span ><img src="' . $img . '" height="50px" width="50px"></span>'
            ?>
          </div>
        </div>

        <div class="signup-form signup-button">
          <input type="submit" value="Xác nhận" name="btnSubmit">
        </div>
      </form>
      <?php
      if (!empty($_POST['btnSubmit'])) {
        $conn = mysqli_connect('localhost', 'root', '', 'ptweb');

        $gender = $gender == 'Nam' ? 1 : 0;
        $department = $department == "Khoa học máy tính" ? "MAT" : "KDL";
        $birth = explode("/", $birth);
        $birth = $birth[2] . '-' . $birth[1] . '-' . $birth[0] . ' 00:00:00';

        $values = "'" . $fullName . "'," . $gender . ",'" . $department . "','" . $birth . "','" . $address . "','" . $img . "'";
        $query = 'insert into student(name,gender,faculty,birthday,address,avartar) values (' . $values . ');';
        
        $result = mysqli_query($conn, $query);
        if (!$result) {
          $message  = 'Invalid query: ' . mysqli_error($conn) . "\n";
          $message .= 'Whole query: ' . $query;
          die($message);
        }
    
        mysqli_close($conn);
    
        header("Location: ./complete_signup.php");
      }
      ?>
    </div>
  </div>
</body>

</html>