<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Document</title>
</head>
<style>
  .background {
    width: 33rem;
    margin: auto;
    margin-top: 2rem;
    padding: 2rem 2rem;
    align-items: center;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }

  .search_layout {
    justify-content: center;
    align-items: center;
  }

  .layout {
    display: flex;
    align-items: center;
    justify-content: center;
    font-size: 16px;
  }

  .search_label {
    width: 85px;
    padding: 5px 6px;
    font-size: 16px;
  }

  .search_input {
    width: 30%;
    height: 2rem;
    padding-left: 0.5rem;
    border-radius: 8px;
    border: 2px solid #4e7aa3;
  }

  select {
    width: 33%;
    height: 2.4rem;
    font-size: 16px;
    padding-left: 0.5rem;
    border-radius: 8px;
    border: 2px solid #4e7aa3;
  }

  .btn {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  .btn-search {
    width: 7rem;
    height: 2.2rem;
    color: white;
    font-size: 16px;
    margin-top: 0.5rem;
    background-color: #079dd9;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }

  .add {
      display: flex;
      align-items: center;
      justify-content: space-between;
      margin-top: 30px;
      font-size: 16px;
    }

  .addNew {
    width: 7rem;
    height: 2.2rem;
    color: white;
    background-color: #079dd9;
    border-radius: 8px;
    border: solid 2px #4e7aa3;
  }

  .search-result {
    display: flex;
    justify-content: flex-start;
  }

  .btn-add {
    display: flex;
    justify-content: flex-end;
  }

  .btn-add1 {
    color: #cfdded;
    background-color: #4f81bd;
    border-radius: 5px;
    border: 2px solid #385e8b;
    height: 30px;
    width: 80px
  }

  .list {
    display: flex;
    flex-direction: column;
  }

  .list-header {
    display: flex;
    justify-content: space-between;
    align-items: center;
    margin-bottom: 6px;
  }

  .list-item {
    display: flex;
    width: 360px;
    justify-content: space-between;
  }

  .list-item-action {
    display: flex;
    justify-content: flex-end;
  }

  .btn-remove {
    margin-right: 10px;
    height: 30px;
  }

  .btn-patch {
    height: 30px;
  }

  .stt {
    width: 10px;
  }

  .name {
    width: 140px;
  }

  .department {
    width: 140px;
  }
</style>

<body>
  <?php
  if (!empty($_POST['btn-add'])) {
    header("Location: ./img_signup.php");
  }
  if (!empty($_POST['btn-search'])) {
  }
  ?>
  <div class="background">
    <form action="" method="POST" id="form" enctype="multipart/form-data">
      <div class="search_layout">
        <div class="layout">
          <p class="search_label">Khoa</p>
          <select name="department" id="department">
            <?php
            $department = array("" => "", "MAT" => "Khoa học máy tính", "KDL" => "Khoa học vật liệu");
            foreach ($department as $key => $value) {
              echo '<option >' . $value . '</option>';
            }
            ?>
          </select>
        </div>
        <div class="layout">
          <p class="search_label">Từ khóa</p>
          <?php
          echo '<input type="text" name="search" id="search" class="search_input"/>'
          ?>
        </div>
      </div>
      <div class="btn">
        <input type="button" name="btn-search" class="btn-search" value="Tìm kiếm" />
      </div>
      <div class=" add">
        <div>
          <p>Số sinh viên tìm thấy: xxx</p>
        </div>

        <div>
          <a href="img_signup.php">
            <input type="submit" class="addNew" value="Thêm" />
          </a>
        </div>
      </div>
      <div class="list-header">
        <div class="list-item">
          <p class="stt">NO</p>
          <p class="name">Tên sinh viên</p>
          <p class="department">Khoa</p>
        </div>
        <div class="list-item-action">
          <p>Action</p>
        </div>
      </div>
      <div class="list">
        <?php
        $conn = mysqli_connect('localhost', 'root', '', 'ptweb');

        $query = "SELECT * FROM student";
        $result = $conn->query($query);
        $data = array();
        while ($row = mysqli_fetch_array($result)) {
          $data[] = $row;
        }
        for ($i = 0; $i < count($data); $i++) {
          echo '
          <div class="list-header">
            <div class="list-item">
              <p class="stt">' . $data[$i]['id'] . '</p>
              <p class="name">' . $data[$i]['name'] . '</p>
              <p class="department">' . $department[$data[$i]['faculty']] . '</p>
            </div>
          <div class="list-item-action">
            <button class="btn-remove">Xoá</button>
            <button class="btn-patch">Sửa</button>
          </div>
        </div>
          ';
        }
        ?>
      </div>
    </form>
  </div>
</body>

</html>